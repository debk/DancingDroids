use std::cmp;


const L: i32 = 8;
const C: i32 = 8;


//1. Écrire une enum Orientation qui gére les 4 cas possibles
enum Orientation { N, S, W, E, }


//Implémentation de l'Orientation
impl Orientation {

  //2. Écrire une fonction qui lit un caractères char et crée une Orientation
  fn affiche(&self) -> &str {
      match self {
          Orientation::N => "N",
          Orientation::S => "S",
          Orientation::W => "W",
          Orientation::E => "E",
      }
  }

  /*3. Écrire un commentaire de documentation de cette fonction qui dit comment l'utiliser
  Orientation::N.affiche();
  Orientation::S.affiche();
  Orientation::W.affiche();
  Orientation::E.affiche();
  */

}

//enum Instruction qui gére les 3 cas possibles
enum Instruction { L, R, F, }


//Implémentation de l'instruction
impl Instruction {

  fn affiche(&self) -> &str {
    match self {
        Instruction::L => "L",
        Instruction::R => "R",
        Instruction::F => "F",
    }
  }
}

/*
  Instruction::L.affiche();
  Instruction::R.affiche();
  Instruction::F.affiche();
*/


//Structure pour utiliser le robot
struct Robot {
  id: usize,
  x: i32,
  y: i32,
  orientation: Orientation,
  instructions: Vec<Instruction>
}


//Implémentation d'un robot
impl Robot {

  fn generation_robot(orientationzer, x, y){
    self.orientation = orientationzer;
    self.x = x;
    self.y = y;
  }



  //a changer fonction juste pour dire qu'elle est la
  fn collision(x, y) -> bool{

    if(x++ != "NULL"){
      return false;
    }

    else true;

  }



  fn action_robot(inst: Instruction){

    //Si on décide de faire une rotation a gauche
    if(inst => Instruction::L){
      //on tourne à gauche
      Orientation::N => Orientation::O;
      Orientation::O => Orientation::S;
      Orientation::S => Orientation::E;
      Orientation::E => Orientation::N;
    } 
  
    //Si on décide de faire une rotation a droite
    if(inst => Instruction::R){
      //on tourne à droite
      Orientation::N => Orientation::E;
      Orientation::E => Orientation::S;
      Orientation::S => Orientation::O;
      Orientation::O => Orientation::N;
    }

    //Si on décide d'avancer en avant
    if(inst => Instruction::F){

      //si on regarde vers le nord
      if(ori => Orientation::N){
        //on avance d'une case vers le haut
        ori => Orientation::N;
        robot.y == y+1;
        //on teste les collisions
        collision(x,y);
      }

      //si on regarde vers le sud
      if(ori => Orientation::S){
        //on avance d'une case vers le bas
        ori => Orientation::S;
        robot.y == y-1;
        //on teste les collisions
        collision(x,y);
      }

      //si on regarde vers l'est
      if(ori => Orientation::E){
        //on avance d'une case vers la droite
        ori => Orientation::E;
        robot.x == x+1;
        //on teste les collisions
        collision(x,y);
      }

      //si on regarde vers le sud
      if(ori => Orientation::O){
        //on avance d'une case vers la gauche
        ori => Orientation::O;
        robot.x == x-1;
        //on teste les collisions
        collision(x,y);
      }
  
    }
  }


  fn affiche_grille(){ //affichage console avec une grille
    let x;
    let y;
    for (x = L-1; x >= 0; --x){
        print!(" -----------------------------------------");
        println!(" {} | ", x);

        //boucle for pour afficher valeurs
        for (y = 0; y < C; ++y){
          print!(" {} |"); //valeurs a afficher (a changer)
        }
        println!("");
    }

    print!(" ------------------------------------------");
    println!("");
    print!("    ");

    for (x = 0; x < L; ++x){
      print!(" {} ", x);
    }
    println!("");
  }
}

  fn main(){

    generation_robot(Orientation::N, 1, 1);
    action_robot(Instruction::R);
    action_robot(Instruction::F);
    affiche_grille();

  }